const db = require('./query');
const express = require('express');
const app = express();

const InitializeRoute = (app, keycloak)=>{
    /**
     * This function comment is parsed by doctrine
     * @route GET /users
     * @group foo - Users information
     * @returns {object} 200 - An array of user info
     * @returns {Error}  default - Unexpected error
     */
    app.get('/users', keycloak.protect(), db.getUsers);
    /**
     * This function comment is parsed by doctrine
     * @route GET /users/:id
     * @group foo - User information
     * @returns {object} 200 - A user info
     * @returns {Error}  default - Unexpected error
     */
    app.get('/users/:id', keycloak.protect(), db.getUserById);
    /**
     * This function comment is parsed by doctrine
     * @route GET /users/details/:id
     * @group foo - User details information
     * @returns {object} 200 - A user info
     * @returns {Error}  default - Unexpected error
     */
    app.get('/users/detail/:id', keycloak.protect(), db.getUserDetailById)
    /**
     * This function comment is parsed by doctrine
     * @route delete /users/:id
     * @group foo - delete user
     */
    app.delete('/users/:id', keycloak.protect(), db.deleteUser);
    /**
     * This function comment is parsed by doctrine
     * @route post /users
     * @group foo - Create user
     */
    app.put('/users/:id', keycloak.protect(), db.updateUser);
    /**
     * This function comment is parsed by doctrine
     * @route post /userdetail
     * @group foo - Create user detail
     */
    app.post('/users', keycloak.protect(), db.createUserDetail);
}
module.exports= InitializeRoute;
