const Pool = require('pg').Pool
require('dotenv').config()
const pool = new Pool({
    user: process.env.DB_USER,
    host: process.env.DB_HOST,
    database: process.env.DB_NAME,
    password: process.env.DB_PASS,
    port: process.env.DB_PORT,
})

const getUsers = (request, response) => {

    const page = parseInt(request.query.page);
    let user = null;

    pool.query('SELECT id, email, first_name, last_name, username ' +
               'FROM keycloack_db."user_entity" LIMIT 10 OFFSET ($1) * 10', [page], (error, results) => {
        if (error) {
            throw error
        }
        console.log(results.rows)
        response.status(200).json(results.rows)
    })
}
const getUserDetailById = (request, response) => {

    const id = request.params.id;
    let user = null;
    
   
        pool.query('SELECT * FROM user_db."USER_DETAIL" WHERE id = $1', [id], (error, detailResults) => {
            if (error) {
                throw error
            }
            
            user = detailResults.rows;
            response.status(200).json(user)
        })
    
}
const getUserById = (request, response) => {

    const id = request.params.id;
    let user = null;
    var idCampus =1;
    pool.query('SELECT id, email, first_name, last_name, username ' +
               'FROM keycloack_db."user_entity" WHERE id = $1', [id], (error, userResults) => {
        if (error) {
            throw error
        }
        user = userResults.rows;
        pool.query('SELECT * FROM user_db."USER_DETAIL" WHERE id = $1', [id], (error, detailResults) => {
            if (error) {
                throw error
            }
            
            user[0]['detail'] = detailResults.rows[0];
            idCampus=user[0]['detail']['idCampus'];
            idEcole=user[0]['detail']['idEcole'];
           
            pool.query('SELECT name FROM organization_db."campus" WHERE id =$1', [idCampus]  ,(error, campusResults) => {
                if (error) {
                    throw error
                }
                user[0]['detail']['Campus']= campusResults.rows[0];
                
                pool.query('SELECT name FROM organization_db."school" WHERE id =$1', [idEcole]  ,(error, schoolResults) => {
                    if (error) {
                        throw error
                    }
                    user[0]['detail']['School']= schoolResults.rows[0];
                    response.status(200).json(user)
                    
                }) 
            }) 
            
           
        })
    })
}
const deleteUser = (request, response) => {
    const id = parseInt(request.params.id)

    pool.query('DELETE FROM keycloack_db."user_entity" WHERE id = $1', [id], (error, results) => {
        if (error) {
            throw error
        }
        pool.query('DELETE FROM user_db."USER_DETAIL" WHERE id = $1', [id], (error, results) => {
            if (error) {
                throw error
            }
            response.status(200).send(`User deleted with ID: ${id}`)
        })
    })
}
const updateUser = (request, response) => {
    const id = parseInt(request.params.id)
    const { email, firstName, lastName, userName, detail } = request.body
    const idDetail = detail.id
    const description = detail.description;
    const photoUrl = detail.photo
    pool.query(
        'UPDATE keycloack_db."user_entity" SET email = $1, first_Name = $2, last_Name = $3, userName = $4 WHERE $5 = id',
        [email, firstName, lastName, userName, idDetail],
        (error, results) => {
            if (error) {
                throw error
            }
            pool.query(
                'UPDATE user_db."USER_DETAIL" SET description = $1 WHERE id = $3',
                [description, photoUrl, idDetail],
                (error, results) => {
                    if (error) {
                        throw error
                    }
                    response.status(200).send(`User modified with ID: ${id}`)
                }
                
            )
        }
    )
}
const createUserDetail = (request, response) => {

    const { id, description, idEcole,idCampus } = request.body;

    pool.query('INSERT INTO user_db."USER_DETAIL" (id, description, "idEcole", "idCampus") VALUES ($1, $2, $3,$4)', [id, description, idEcole,idCampus], (error, results) => {
        if (error) {
            throw error
        }
        response.status(201).send(`UserDetail added with ID: ${results.insertId}`)
    })
}
module.exports = {
    getUsers,
    getUserDetailById,
    getUserById,
    deleteUser,
    updateUser,
    createUserDetail
}
