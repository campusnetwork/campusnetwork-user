require('dotenv').config()

const express = require('express');
const Keycloak = require('keycloak-connect');
const session = require('express-session');
const expressHbs = require('express-handlebars');
const bodyParser = require('body-parser');
const app = express();
const host = process.env.APP_HOST;
const port = process.env.APP_PORT;
const route = require('./router');
const expressSwagger = require('express-swagger-generator')(app);

// Register 'handelbars' extension with The Mustache Express
app.engine('hbs', expressHbs({extname:'hbs',
  defaultLayout:'layout.hbs',
  relativeTo: __dirname}));
app.set('view engine', 'hbs');


var memoryStore = new session.MemoryStore();
var keycloak = new Keycloak({ store: memoryStore });

//session
app.use(session({
    secret:'thisShouldBeLongAndSecret',
    resave: false,
    saveUninitialized: true,
    store: memoryStore
}));
  
app.use(keycloak.middleware());

app.use(
    bodyParser.urlencoded({
        extended: true,
    })
);
app.use(bodyParser.json());

app.use( keycloak.middleware( { logout: '/'} ));

route(app, keycloak);

let options = {
    swaggerDefinition: {
        info: {
            description: 'Api-User du réseau social CampusNetwork',
            title: 'Api-User CampusNetwork',
            version: '1.0.0',
        },
        host: '127.0.0.1:3001',
        basePath: '/v1',
        produces: [
            "application/json",
            "application/xml"
        ],
        schemes: ['http', 'https'],
        securityDefinitions: {
            JWT: {
                type: 'apiKey',
                in: 'header',
                name: 'Authorization',
                description: "",
            }
        }
    },
    basedir: __dirname, //app absolute path
    files: ['./router.js'] //Path to the API handle folder
};
expressSwagger(options);

app.listen(port, () => {
    console.log(`Server running on ${host}:${port}.`)
})
