# API User

<img src="https://i.ibb.co/6R8FSVQ/sezegzaeg.png"></img>

CampusNetwork est un réseau social inter campus visant à simplifier les création d'évennements entre écoles et campus.

L'Api User est une des apis du réseau social CampusNetwork. Il a pour but de récupérer les informations
relatives aux évennements dans l'application.

L'application est actuellement disponible sur https://user.campusnetwork.mohamedhenni.wtf/.

# Project Title

Campusnetwork-user

## Getting Started

Cette api gère toute la partie user de l'application.

### Prerequisites

Pour que l'api fonctionne elle nécessite plusieur dépendances dont le framework express, elle nécessite aussi une connection keycloak. Il doit aussi avoir installer node sur la machine.

### CONFIG 

Pour que l'appli fonctionne créer un .env avec les valeurs suivantes 

APP_HOST=???
APP_PORT=????
DB_USER=????
DB_HOST=????
DB_PASS=????
DB_PORT=????
DB_NAME=????



### Installing

git clone git@gitlab.com:campusnetwork/campusnetwork-user.git
cd campusnetwork-user
npm install

### Using 

node server.js


## Deployment

Prêt à être conteneurisé avec [docker](https://www.docker.com/)

Pour cela mettre la variable du .env APP_HOST=0.0.0.0

Puis lancer la commande depuis la racine du projet

docker build -t [imageName] .

## Built With

express
keycloak-connect
express-session
express-handlebars
body-parser
dotenv


## Authors

* **Berthomé théo** - *Initial work* -




